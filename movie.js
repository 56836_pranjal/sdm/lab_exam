const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

// display all data of employee
router.get("/:name", (request, response) => {
  const { name } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from movie where
        moviename = '${moviename}'
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("user not found !");
    }
  });
});

router.post("/add", (request, response) => {
  const { moviename, releasedate, time,director } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into movie
          (moviename, releasedate, time,director)
        values
          ( '${moviename}','${releasedate}','${time}','${director}')
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:name", (request, response) => {
  const {moviename} = request.params;
  const { releasedate } = request.body;
  const {time} = request.body;
  const statement = `
    update movie
    set
    releasedate='${releasedate}'
    time='${time}'
    where
      moviename = '${moviename}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});
router.delete("/remove/:name", (request, response) => {
  const { name } = request.params;
  const statement = `
    delete from movie
    where
      moviename = '${moviename}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});
module.exports = router;
